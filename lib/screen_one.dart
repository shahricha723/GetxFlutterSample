import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_demo/screen_two.dart';

class LoginScreen extends StatefulWidget {
  final String name;

  const LoginScreen({Key? key, this.name = ''}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(" Welcome Screen One"+widget.name),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                  onPressed: () {
                    // Navigator.pop(context);
                    Get.to(ScreenTwo());
                  },
                  child: const Text("Go to Next"))
            ],
          ),
        ));
  }
}
