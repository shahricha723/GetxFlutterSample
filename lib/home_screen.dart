import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_demo/screen_one.dart';

class HomeScreen extends StatefulWidget {
  // const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            Card(
              child: ListTile(
                title: const Text("Navigation"),
                subtitle: const Text("Demo Navigation "),
                onTap: () {
                  Get.to(const LoginScreen(name: "Richa",));

                },
              ),
            ),
            Card(
              child: ListTile(
                title: const Text("Getx Dialog Alert"),
                subtitle: const Text("Dialog Alert with Getx "),
                onTap: () {
                  Get.defaultDialog(
                      title: "Delete Chat",
                      middleText: "Are you sure you want to delete the chat?",
                      contentPadding: const EdgeInsets.all(20),
                      titlePadding: const EdgeInsets.only(top: 20),
                      textConfirm: "Yes",
                      onConfirm: Get.back,
                      textCancel: "No");
                },
              ),
            ),
            Card(
              child: ListTile(
                title: const Text("Getx BottomSheet Alert"),
                subtitle: const Text("BottomSheet Alert with Getx "),
                onTap: () {
                  Get.bottomSheet(Container(
                    decoration: BoxDecoration(
                        color: Colors.greenAccent,
                        borderRadius: BorderRadius.circular(6)),
                    child: Column(
                      children: [
                        ListTile(
                            title: Text("Dark Theme"),
                            leading: Icon(Icons.dark_mode),
                            onTap: () {
                              Get.changeTheme(ThemeData.dark());
                              Get.back();
                            }),
                        ListTile(
                            title: Text("Light Theme"),
                            leading: Icon(Icons.light_mode),
                            onTap: () {
                              Get.changeTheme(ThemeData.light());
                              Get.back();
                            })
                      ],
                    ),
                  ));
                },
              ),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: const Text("Add"),
          onPressed: () {
            Get.snackbar("Snack", "Clicked",
                colorText: Colors.white,
                backgroundColor: Colors.red,
                snackPosition: SnackPosition.BOTTOM,
                titleText: const Text("Demo"));
          },
        ));
  }
}
